package io.github.junxworks.shardingsphere.datamigrate.core;

import java.util.concurrent.CountDownLatch;

public class Signal {
	private volatile boolean started = false;

	private volatile boolean shutdown = false;

	private boolean suspend = false;

	private CountDownLatch latch;

	public boolean isStarted() {
		return started;
	}

	public void start() {
		if (started) {
			throw new UnsupportedOperationException("Engine is already started.");
		}
		started = true;
		shutdown = false;
	}

	public void shutdown() {
		if (shutdown) {
			throw new UnsupportedOperationException("Engine is already shutdown.");
		}
		shutdown = true;
		started = false;
	}

	public boolean isShutdown() {
		return shutdown;
	}

	public boolean isSuspend() {
		return suspend;
	}

	public void suspend() {
		if (suspend) {
			throw new UnsupportedOperationException("Engine is already suspended.");
		}
		this.suspend = true;
		this.latch = new CountDownLatch(1);
	}

	public void resume() {
		if (!suspend) {
			throw new UnsupportedOperationException("Engine is not suspended.");
		}
		if (this.latch != null) {
			this.latch.countDown();
		}
		suspend = false;
	}

	public void haveARest() {
		if (this.latch != null) {
			try {
				this.latch.await();
			} catch (InterruptedException e) {
			}
		}
	}

}
