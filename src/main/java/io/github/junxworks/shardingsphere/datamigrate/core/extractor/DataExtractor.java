package io.github.junxworks.shardingsphere.datamigrate.core.extractor;

import java.sql.SQLException;

public interface DataExtractor {

	/**
	 * 根据上下文配置抽取数据，然后一条一条的发送出来
	 *
	 * @param context the context
	 */
	void extract(ExtractContext context) throws SQLException;
}
