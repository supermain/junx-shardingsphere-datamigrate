package io.github.junxworks.shardingsphere.datamigrate.core.extractor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import io.github.junxworks.ep.core.ds.DynamicDataSource;
import io.github.junxworks.junx.core.exception.BaseRuntimeException;
import io.github.junxworks.junx.core.util.StringUtils;
import io.github.junxworks.junx.event.EventBus;
import io.github.junxworks.junx.event.EventContext;
import io.github.junxworks.shardingsphere.datamigrate.core.Params;
import io.github.junxworks.shardingsphere.datamigrate.core.Signal;

@Component("DBDataExtractor")
public class DBDataExtractor implements DataExtractor {

	private Logger log = LoggerFactory.getLogger(DBDataExtractor.class);

	@Autowired
	private Signal signal;

	@Autowired
	private EventBus eventBus;

	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;

	@Autowired
	private DynamicDataSource dataSource;

	@Override
	public void extract(ExtractContext context) throws SQLException {
		String topic = context.getTopic();
		String targetTable = context.getTargetTable();
		String primaryKey = context.getPrimaryKey();
		if (StringUtils.isNull(topic)) {
			throw new BaseRuntimeException("Topic can not be empty");
		}
		if (StringUtils.isNull(targetTable)) {
			throw new BaseRuntimeException("Target table can not be empty");
		}
		if (StringUtils.isNull(primaryKey)) {
			throw new BaseRuntimeException("Primary key can not be empty");
		}
		String sql = null;
		Map<String, Object> params = Maps.newHashMap();
		log.info(targetTable + "表数据正在统计中......");
		int capacity = context.getCapacity();
		if (capacity == -1) {
			sql = "select count(1) from (" + context.getSql() + ") a";
			Integer count = jdbcTemplate.queryForObject(sql, params, Integer.class);
			if (count == 0) {
				return;
			}
			capacity = count;
		}
		log.info(targetTable + "表数据抽取事件共" + capacity + "条记录待发送");
		int pageSize = context.getPageSize();
		boolean clearKeyValue = context.isClearPrimaryKeyValue();//是否去掉主键值，默认是
		try (Connection conn = dataSource.getPrimaryDataSource().getConnection(); PreparedStatement ps = conn.prepareStatement(context.getSql(), ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);) {
			ps.setFetchSize(Integer.MAX_VALUE);
			log.info(targetTable + "表数据正在查询中......");
			ResultSet rs = ps.executeQuery();
			log.info(targetTable + "表数据查询完毕，准备发送！");
			ResultSetMetaData md = rs.getMetaData();
			int columnCount = md.getColumnCount();
			int currentSize = 0;
			int totalCount = 0;
			List<Map<String, Object>> resToSend = Lists.newArrayListWithCapacity(pageSize);
			while (rs.next()) {
				if (signal.isShutdown()) {
					break;
				}
				if (signal.isSuspend()) {
					signal.haveARest();
				}
				Map<String, Object> map = Maps.newHashMap();
				for (int i = 1; i <= columnCount; i++) {
					String columnName = md.getColumnName(i);
					if (!(clearKeyValue && primaryKey.equalsIgnoreCase(columnName))) {
						map.put(columnName, rs.getObject(i));
					}
				}
				resToSend.add(map);
				if (++currentSize == pageSize) {
					currentSize = 0;
					totalCount += pageSize;
					sendData(topic, targetTable, resToSend, totalCount);
					resToSend = Lists.newArrayListWithCapacity(pageSize);
				}
			}
			if (signal.isShutdown()) {
				return;
			}
			if (!resToSend.isEmpty()) {
				sendData(topic, targetTable, resToSend, capacity);
			}
		}
		log.info(targetTable + "表数据抽取事件已发送完毕！共" + capacity + "条记录。");
	}

	private void sendData(String topic, String targetTable, List<Map<String, Object>> resToSend, int currentSize) {
		EventContext ctx = new EventContext(topic);
		ctx.setData(Params.PARAM_EXTRACT_OBJS, resToSend);
		ctx.setData(Params.PARAM_TABLE_NAME, targetTable);
		try {
			eventBus.publish(ctx);
		} catch (Exception e) {
			throw new RuntimeException(targetTable + "发送数据抽取事件失败", e);
		}
		log.info(targetTable + "数据抽取事件当前发送记录数" + currentSize);
	}
}
