package io.github.junxworks.shardingsphere.datamigrate.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.jdbc.SQL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Maps;

import io.github.junxworks.ep.core.ds.annotation.DS;
import io.github.junxworks.junx.core.util.StringUtils;
import io.github.junxworks.shardingsphere.datamigrate.Constants;
import io.github.junxworks.shardingsphere.datamigrate.config.MigrateConfig;
import io.github.junxworks.shardingsphere.datamigrate.service.DataStoreService;

@Service
public class DataStoreServiceImpl implements DataStoreService {

	@Autowired
	private MigrateConfig config;

	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;

	private Map<String, String> cache = Maps.newHashMap();

	@SuppressWarnings("unchecked")
	@Override
	@DS(Constants.DS_TARGET)
	@Transactional
	public int insertBatch(String tableName, List<Map<String, Object>> objs) {
		if (objs == null || objs.isEmpty()) {
			return 0;
		}
		String sql = getInsertSql(tableName, objs.get(0));
		int batchSize = config.getWriteBatchSize();
		for (int page = 1, index = 0, len = objs.size(); index < len; page++) {
			int toIndex = page * batchSize;
			if (toIndex >= len) {
				toIndex = len;
			}
			List<Map<String, Object>> subList = objs.subList(index, toIndex);
			Map<String, Object>[] params = subList.toArray(new Map[0]);
			jdbcTemplate.batchUpdate(sql, subList.toArray(params));
			index = toIndex;
		}
		return objs.size();
	}

	public String getInsertSql(String tableName, Map<String, Object> row) {
		String sql = cache.get(tableName);
		if (StringUtils.isNull(sql)) {
			synchronized (cache) {
				sql = cache.get(tableName);
				if (StringUtils.isNull(sql)) {
					SQL s = new SQL();
					s.INSERT_INTO(tableName);
					row.entrySet().forEach(r -> {
						s.VALUES(r.getKey(), ":" + r.getKey());
					});
					sql = s.toString();
					cache.put(tableName, sql);
				}
			}
		}
		return sql;
	}

}
