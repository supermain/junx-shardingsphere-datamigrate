package io.github.junxworks.shardingsphere.datamigrate.core.handler;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.github.junxworks.junx.core.util.StringUtils;
import io.github.junxworks.junx.event.EventChannel;
import io.github.junxworks.junx.event.EventChannelHandler;
import io.github.junxworks.junx.event.EventContext;
import io.github.junxworks.shardingsphere.datamigrate.core.Params;
import io.github.junxworks.shardingsphere.datamigrate.service.DataStoreService;

@Component
public class DataStoreHandler implements EventChannelHandler {
	private static final Logger log = LoggerFactory.getLogger(DataStoreHandler.class);

	@Autowired
	private DataStoreService dataService;

	@SuppressWarnings("unchecked")
	@Override
	public void handleEvent(EventContext event, EventChannel channel) throws Exception {
		String tableName = event.getData(String.class, Params.PARAM_TABLE_NAME);
		if (StringUtils.notNull(tableName)) {
			try {
				List<Map<String, Object>> objs = (List<Map<String, Object>>) event.getData(Params.PARAM_EXTRACT_OBJS);
				dataService.insertBatch(tableName, objs);
			} catch (Exception e) {
				log.error(tableName + "存储数据失败", e);
			}
		}
	}

}
