package io.github.junxworks.shardingsphere.datamigrate.service;

import java.util.List;
import java.util.Map;

public interface DataStoreService {

	int insertBatch(String tableName, List<Map<String, Object>> objs);

}
